package org.epis.signpdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.FileOutputStream;
import java.io.IOException;

public class AlterPDF
{
    public static void main(String[] args)
    {
        try
        {
            PdfReader pdfReader = new PdfReader("src/main/resources/hello_signed.pdf");
            PdfStamper pdfStamper = new PdfStamper(pdfReader, 
                    new FileOutputStream("results/hello_modified.pdf"));

            BaseFont bf = BaseFont.createFont(
                BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED); // set font

            //loop on pages (1-based)
            for (int i = 1; i <= pdfReader.getNumberOfPages(); i++)
            {
                // get object for writing over the existing content;
                // you can also use getUnderContent for writing in the bottom layer
                PdfContentByte over = pdfStamper.getOverContent(i);

                // write text
                over.beginText();
                over.setFontAndSize(bf, 10);    // set font and size
                over.setTextMatrix(200, 700);   // set x,y position (0,0 is at the bottom left)
                over.showText("PDF Modificado");  // set text
                over.endText();
            }

            pdfStamper.close();
        } catch (IOException | DocumentException e) {
            
        }
    }
}
