# Digital Signature for PDF

Samples and Testing using iText 5

[More details here](http://developers.itextpdf.com/examples/security-itext5/digital-signatures-white-paper)

- Create a keystore with keytool
```sh
keytool -genkey -alias demo -keyalg RSA -keysize 2048 -keystore ks
```
